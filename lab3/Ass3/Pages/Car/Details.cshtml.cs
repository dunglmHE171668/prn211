﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Ass3.Models;

namespace Ass3.Pages.Car
{
    public class DetailsModel : PageModel
    {
        private readonly Ass3.Models.MyStockContext _context;

        public DetailsModel(Ass3.Models.MyStockContext context)
        {
            _context = context;
        }

        public Ass3.Models.Car Car { get; set; } = default!;
        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.Cars == null)
            {
                return NotFound();
            }

            var car = await _context.Cars.FirstOrDefaultAsync(m => m.CarId == id);
            if (car == null)
            {
                return NotFound();
            }
            else 
            {
                Car = car;
            }
            return Page();
        }
    }
}
