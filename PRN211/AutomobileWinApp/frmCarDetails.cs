using AutomobileLibrary.Repository;
using AutomobileLibrary.BussinessObject;
namespace AutomobileWinApp
{
    public partial class frmCarDetails : Form
    {
        public frmCarDetails()
        {
            InitializeComponent();
        }
        public ICarRepository CarRepository { get; set; }
        public bool InsertOrUpdate { get; set; }
        public Car CarInfo { get; set; }


        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }


        private void btnSave_Click(object sender, EventArgs e)
        {
           
                var car = new Car
                {
                    CarId = int.Parse(txtCarID.Text),
                    CarName = txtCarName.Text,
                    Manufacturer = cboManufacturer.Text,
                    Price = decimal.Parse(txtPrice.Text),
                    ReleaseYear = int.Parse(txtReleaseYear.Text),
                };
                if (InsertOrUpdate == false)
                {
                    CarRepository.InsertCar(car);
                    
                }
                else
                {
                    CarRepository.UpdateCar(car);
                    
                }
                Close();
        }
            
        

        private void Form1_Load(object sender, EventArgs e)
        {
            // Check if combo box has items before setting the selected index

            cboManufacturer.SelectedIndex = 0;


            txtCarID.Enabled = !InsertOrUpdate;

            if (InsertOrUpdate == true)//update mode
            {
                txtCarID.Text = CarInfo.CarId.ToString();
                txtCarName.Text = CarInfo.CarName;
                txtPrice.Text = CarInfo.Price.ToString();
                txtReleaseYear.Text = CarInfo.ReleaseYear.ToString();
                cboManufacturer.Text = CarInfo.Manufacturer.Trim();
                
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}