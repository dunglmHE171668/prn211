﻿namespace AutomobileWinApp
{
    partial class frmCarDetails
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lbCarID = new Label();
            txtCarID = new TextBox();
            lbCarName = new Label();
            txtCarName = new TextBox();
            label5 = new Label();
            lbReleasesYear = new Label();
            lbPrice = new Label();
            lbManufacturer = new Label();
            cboManufacturer = new ComboBox();
            txtPrice = new MaskedTextBox();
            txtReleaseYear = new MaskedTextBox();
            btnSave = new Button();
            btnCancel = new Button();
            SuspendLayout();
            // 
            // lbCarID
            // 
            lbCarID.AutoSize = true;
            lbCarID.Location = new Point(154, 116);
            lbCarID.Name = "lbCarID";
            lbCarID.Size = new Size(50, 20);
            lbCarID.TabIndex = 1;
            lbCarID.Text = "Car ID";
            // 
            // txtCarID
            // 
            txtCarID.Location = new Point(224, 116);
            txtCarID.Name = "txtCarID";
            txtCarID.Size = new Size(210, 27);
            txtCarID.TabIndex = 1;
            // 
            // lbCarName
            // 
            lbCarName.AutoSize = true;
            lbCarName.Location = new Point(129, 159);
            lbCarName.Name = "lbCarName";
            lbCarName.Size = new Size(75, 20);
            lbCarName.TabIndex = 1;
            lbCarName.Text = "Car Name";
            // 
            // txtCarName
            // 
            txtCarName.Location = new Point(224, 159);
            txtCarName.Name = "txtCarName";
            txtCarName.Size = new Size(210, 27);
            txtCarName.TabIndex = 2;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(154, 296);
            label5.Name = "label5";
            label5.Size = new Size(50, 20);
            label5.TabIndex = 1;
            label5.Text = "label2";
            // 
            // lbReleasesYear
            // 
            lbReleasesYear.AutoSize = true;
            lbReleasesYear.Location = new Point(103, 296);
            lbReleasesYear.Name = "lbReleasesYear";
            lbReleasesYear.Size = new Size(101, 20);
            lbReleasesYear.TabIndex = 1;
            lbReleasesYear.Text = "Released year";
            // 
            // lbPrice
            // 
            lbPrice.AutoSize = true;
            lbPrice.Location = new Point(154, 248);
            lbPrice.Name = "lbPrice";
            lbPrice.Size = new Size(41, 20);
            lbPrice.TabIndex = 1;
            lbPrice.Text = "Price";
            // 
            // lbManufacturer
            // 
            lbManufacturer.AutoSize = true;
            lbManufacturer.Location = new Point(106, 207);
            lbManufacturer.Name = "lbManufacturer";
            lbManufacturer.Size = new Size(98, 20);
            lbManufacturer.TabIndex = 1;
            lbManufacturer.Text = "ManuFacturer";
            // 
            // cboManufacturer
            // 
            cboManufacturer.BackColor = SystemColors.ScrollBar;
            cboManufacturer.FormattingEnabled = true;
            cboManufacturer.Items.AddRange(new object[] { "Audi", "Messedes", "Roll royce" });
            cboManufacturer.Location = new Point(225, 207);
            cboManufacturer.Name = "cboManufacturer";
            cboManufacturer.Size = new Size(209, 28);
            cboManufacturer.TabIndex = 3;
            // 
            // txtPrice
            // 
            txtPrice.Location = new Point(225, 248);
            txtPrice.Name = "txtPrice";
            txtPrice.Size = new Size(209, 27);
            txtPrice.TabIndex = 4;
            txtPrice.MaskInputRejected += maskedTextBox1_MaskInputRejected;
            // 
            // txtReleaseYear
            // 
            txtReleaseYear.Location = new Point(224, 289);
            txtReleaseYear.Name = "txtReleaseYear";
            txtReleaseYear.Size = new Size(209, 27);
            txtReleaseYear.TabIndex = 5;
            txtReleaseYear.MaskInputRejected += maskedTextBox1_MaskInputRejected;
            // 
            // btnSave
            // 
            btnSave.Location = new Point(224, 346);
            btnSave.Name = "btnSave";
            btnSave.Size = new Size(94, 29);
            btnSave.TabIndex = 5;
            btnSave.Text = "Save";
            btnSave.UseVisualStyleBackColor = true;
            btnSave.Click += btnSave_Click;
            // 
            // btnCancel
            // 
            btnCancel.Location = new Point(340, 346);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(94, 29);
            btnCancel.TabIndex = 5;
            btnCancel.Text = "Cancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // frmCarDetails
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(btnCancel);
            Controls.Add(btnSave);
            Controls.Add(txtReleaseYear);
            Controls.Add(txtPrice);
            Controls.Add(cboManufacturer);
            Controls.Add(lbReleasesYear);
            Controls.Add(label5);
            Controls.Add(lbPrice);
            Controls.Add(txtCarName);
            Controls.Add(lbManufacturer);
            Controls.Add(lbCarName);
            Controls.Add(txtCarID);
            Controls.Add(lbCarID);
            Name = "frmCarDetails";
            Text = "frmCarDetails";
            Load += Form1_Load;
            ResumeLayout(false);
            PerformLayout();
        }



        #endregion
        private Label lbCarID;
        private TextBox txtCarID;
        private Label lbCarName;
        private TextBox txtCarName;
        private Label label5;
        private Label lbReleasesYear;
        private Label lbPrice;
        private Label lbManufacturer;
        private ComboBox cboManufacturer;
        private MaskedTextBox txtPrice;
        private MaskedTextBox txtReleaseYear;
        private Button btnSave;
        private Button btnCancel;
    }
}