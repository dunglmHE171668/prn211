﻿namespace AutomobileWinApp
{
    partial class frmCarManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnNew = new Button();
            btnLoad = new Button();
            txtReleaseYear = new MaskedTextBox();
            txtPrice = new MaskedTextBox();
            lbReleasesYear = new Label();
            label5 = new Label();
            lbPrice = new Label();
            txtCarName = new TextBox();
            lbManufacturer = new Label();
            lbCarName = new Label();
            txtCarID = new TextBox();
            lbCarID = new Label();
            txtManufacturer = new TextBox();
            btnDelete = new Button();
            dgvCarList = new DataGridView();
            btnClose = new Button();
            ((System.ComponentModel.ISupportInitialize)dgvCarList).BeginInit();
            SuspendLayout();
            // 
            // btnNew
            // 
            btnNew.Location = new Point(261, 122);
            btnNew.Margin = new Padding(3, 2, 3, 2);
            btnNew.Name = "btnNew";
            btnNew.Size = new Size(111, 22);
            btnNew.TabIndex = 16;
            btnNew.Text = "New";
            btnNew.UseVisualStyleBackColor = true;
            btnNew.Click += btnNew_Click;
            // 
            // btnLoad
            // 
            btnLoad.Location = new Point(102, 122);
            btnLoad.Margin = new Padding(3, 2, 3, 2);
            btnLoad.Name = "btnLoad";
            btnLoad.Size = new Size(106, 22);
            btnLoad.TabIndex = 17;
            btnLoad.Text = "Load";
            btnLoad.UseVisualStyleBackColor = true;
            btnLoad.Click += btnLoad_Click;
            // 
            // txtReleaseYear
            // 
            txtReleaseYear.Location = new Point(437, 49);
            txtReleaseYear.Margin = new Padding(3, 2, 3, 2);
            txtReleaseYear.Name = "txtReleaseYear";
            txtReleaseYear.Size = new Size(183, 23);
            txtReleaseYear.TabIndex = 18;
            // 
            // txtPrice
            // 
            txtPrice.Location = new Point(438, 18);
            txtPrice.Margin = new Padding(3, 2, 3, 2);
            txtPrice.Name = "txtPrice";
            txtPrice.Size = new Size(183, 23);
            txtPrice.TabIndex = 15;
            // 
            // lbReleasesYear
            // 
            lbReleasesYear.AutoSize = true;
            lbReleasesYear.Location = new Point(331, 54);
            lbReleasesYear.Name = "lbReleasesYear";
            lbReleasesYear.Size = new Size(78, 15);
            lbReleasesYear.TabIndex = 6;
            lbReleasesYear.Text = "Released year";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(375, 54);
            label5.Name = "label5";
            label5.Size = new Size(38, 15);
            label5.TabIndex = 7;
            label5.Text = "label2";
            // 
            // lbPrice
            // 
            lbPrice.AutoSize = true;
            lbPrice.Location = new Point(375, 18);
            lbPrice.Name = "lbPrice";
            lbPrice.Size = new Size(33, 15);
            lbPrice.TabIndex = 8;
            lbPrice.Text = "Price";
            // 
            // txtCarName
            // 
            txtCarName.Location = new Point(102, 50);
            txtCarName.Margin = new Padding(3, 2, 3, 2);
            txtCarName.Name = "txtCarName";
            txtCarName.Size = new Size(184, 23);
            txtCarName.TabIndex = 13;
            // 
            // lbManufacturer
            // 
            lbManufacturer.AutoSize = true;
            lbManufacturer.Location = new Point(-1, 86);
            lbManufacturer.Name = "lbManufacturer";
            lbManufacturer.Size = new Size(81, 15);
            lbManufacturer.TabIndex = 9;
            lbManufacturer.Text = "ManuFacturer";
            // 
            // lbCarName
            // 
            lbCarName.AutoSize = true;
            lbCarName.Location = new Point(19, 50);
            lbCarName.Name = "lbCarName";
            lbCarName.Size = new Size(60, 15);
            lbCarName.TabIndex = 10;
            lbCarName.Text = "Car Name";
            // 
            // txtCarID
            // 
            txtCarID.Location = new Point(102, 18);
            txtCarID.Margin = new Padding(3, 2, 3, 2);
            txtCarID.Name = "txtCarID";
            txtCarID.Size = new Size(184, 23);
            txtCarID.TabIndex = 11;
            // 
            // lbCarID
            // 
            lbCarID.AutoSize = true;
            lbCarID.Location = new Point(41, 18);
            lbCarID.Name = "lbCarID";
            lbCarID.Size = new Size(39, 15);
            lbCarID.TabIndex = 12;
            lbCarID.Text = "Car ID";
            // 
            // txtManufacturer
            // 
            txtManufacturer.Location = new Point(102, 86);
            txtManufacturer.Margin = new Padding(3, 2, 3, 2);
            txtManufacturer.Name = "txtManufacturer";
            txtManufacturer.Size = new Size(184, 23);
            txtManufacturer.TabIndex = 13;
            // 
            // btnDelete
            // 
            btnDelete.Location = new Point(437, 122);
            btnDelete.Margin = new Padding(3, 2, 3, 2);
            btnDelete.Name = "btnDelete";
            btnDelete.Size = new Size(111, 22);
            btnDelete.TabIndex = 16;
            btnDelete.Text = "Delete";
            btnDelete.UseVisualStyleBackColor = true;
            btnDelete.Click += btnDelete_Click;
            // 
            // dgvCarList
            // 
            dgvCarList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvCarList.Location = new Point(19, 148);
            dgvCarList.Margin = new Padding(3, 2, 3, 2);
            dgvCarList.Name = "dgvCarList";
            dgvCarList.RowHeadersWidth = 51;
            dgvCarList.RowTemplate.Height = 29;
            dgvCarList.Size = new Size(609, 155);
            dgvCarList.TabIndex = 19;
            dgvCarList.CellDoubleClick += DgvCarList_CellDoubleClick;
            // 
            // btnClose
            // 
            btnClose.Location = new Point(261, 308);
            btnClose.Margin = new Padding(3, 2, 3, 2);
            btnClose.Name = "btnClose";
            btnClose.Size = new Size(111, 22);
            btnClose.TabIndex = 16;
            btnClose.Text = "Close";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // frmCarManagement
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(700, 338);
            Controls.Add(dgvCarList);
            Controls.Add(btnDelete);
            Controls.Add(btnClose);
            Controls.Add(btnNew);
            Controls.Add(btnLoad);
            Controls.Add(txtReleaseYear);
            Controls.Add(txtPrice);
            Controls.Add(lbReleasesYear);
            Controls.Add(label5);
            Controls.Add(lbPrice);
            Controls.Add(txtManufacturer);
            Controls.Add(txtCarName);
            Controls.Add(lbManufacturer);
            Controls.Add(lbCarName);
            Controls.Add(txtCarID);
            Controls.Add(lbCarID);
            Margin = new Padding(3, 2, 3, 2);
            Name = "frmCarManagement";
            Text = "frmCarManagement";
            Load += frmCarManagement_Load;
            ((System.ComponentModel.ISupportInitialize)dgvCarList).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnNew;
        private Button btnLoad;
        private MaskedTextBox txtReleaseYear;
        private MaskedTextBox txtPrice;
        private Label lbReleasesYear;
        private Label label5;
        private Label lbPrice;
        private TextBox txtCarName;
        private Label lbManufacturer;
        private Label lbCarName;
        private TextBox txtCarID;
        private Label lbCarID;
        private TextBox txtManufacturer;
        private Button btnDelete;
        private DataGridView dgvCarList;
        private Button btnClose;
    }
}