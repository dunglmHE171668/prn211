﻿namespace PRN211
{
    internal class Program
    {
        static void SumArray(out int s, params int[] list)
        {
            int i;
            s = 0;
            for (i = 0; i < list.Length; i++)
            { s += list[i]; }
        }
        static int[] numbers = { 1, 2, 3, 4, 5 };
        static ref int FindNumber(int target)
        {
            bool flag = true;
            ref int value = ref numbers[0];
            for(int ctr=0; ctr<numbers.Length && flag == true; ctr++)
            {
                if (numbers[ctr] == target)
                {
                    value = ref numbers[ctr];
                    flag = false;
                }
            }
            return ref value;
        }
        static void Main(string[] args)
        {
            /* --double salary = 200.234;
             string name = "soren";
             string str1 = string.Format("Name{0,6}, Salary{1,2:N2}",name, salary);
             Console.WriteLine(str1);
             string str2 = $"Name{name,7}"*/

            /*--double d;
            int i;
            string s;
            DateTime date;
            Console.WriteLine("****Data type pasrsing******");
            Console.Write("Enter a string value: ");
            s = Console.ReadLine();
            Console.WriteLine("Enter a double value: ");
            d = double.Parse(Console.ReadLine());
            Console.Write("Enter interger value: ");
            i = int.Parse(Console.ReadLine());
            Console.Write("Enter date value: ");
            date = DateTime.Parse(Console.ReadLine());
            Console.WriteLine($"vlaue of s:{s}, i: {i}, d: {d}, date: {date:dd/MM/yyyy}");
            Console.ReadLine();*/
            /*Console.WriteLine("****Use Digit Separators****");
            Console.Write("Integer: ");
            Console.WriteLine(123-456);
            Console.Write("Double: ");
            Console.WriteLine(123 - 456.12);
            Console.Write("Hex; ");
            Console.WriteLine("0x_00_00_FF");
            Console.WriteLine("***** use binary literials ****");
            Console.WriteLine("Sixteen: {0}", 0b_0001_0000);
            Console.WriteLine("Thirty Two: {0}, 0b_0010_0000");
            Console.WriteLine("Six four: {0}", 0b_0100_0000);
            Console.ReadLine();*/


            /*-- int s;
             SumArray(out s, 1, 2, 3, 4);
             Console.WriteLine($"s={s}");
             int[] myIntArray = { 5, 6, 7, 8, 9 };
             SumArray(out s, myIntArray);
             Console.WriteLine($"s={s}");
             SumArray(out s);
             Console.WriteLine($"s={s}");
             Console.ReadLine();*/

            /* Console.Write("Orginal sequence: ");
             Console.WriteLine(string.Join(" ",numbers));
             ref int value = ref FindNumber(3);
             value *= 2;
             Console.Write("New sequence: ");
             Console.WriteLine(string.Join(" ", numbers));
             Console.ReadLine();*/

            /*(string MyString, int MyNumber) Myvalues = ("Hello world!", 2050);
            Console.WriteLine($"MyString: {Myvalues.MyString}");
            Console.WriteLine($"MyNumber: {Myvalues.MyNumber}");
            Console.ReadLine();*/

            /* string[] stringArray = { "12", "Hello", "3.14", "20" };
             for(int i=0;i<stringArray.Length;i++)
             {
                 if (int.TryParse(stringArray[i], out _))
                 Console.WriteLine($"{stringArray[i]}: valid");
                 else
                     Console.WriteLine($"{stringArray[i]}: invalid");
             }
             Console.ReadLine();*/

            Console.Write("Input Data: ");
            int.TryParse(Console.ReadLine(), out int n);
            if(n is int count && count>0)
            {
                Console.WriteLine(new string('*', count));
            }
            else
            {
                Console.WriteLine("Data invalid");
            }
            Console.ReadLine();
        }
    }
}
