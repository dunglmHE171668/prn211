﻿using System;

namespace BusinessObject
{
    public class MemberObject
    {
        public int MemberId { get; set; }
        
        public String Email { get; set; }

        public String CompanyName { get; set; }
        public String Password { get; set; }
        public String City { get; set; }
        public String Country { get; set; }

        public override string ToString()
        {
            return $"{{{nameof(MemberId)}={MemberId.ToString()}, {nameof(CompanyName)}={CompanyName}, {nameof(Email)}={Email}, {nameof(Password)}={Password}, {nameof(City)}={City}, {nameof(Country)}={Country}, }}";
        }
    }
}
