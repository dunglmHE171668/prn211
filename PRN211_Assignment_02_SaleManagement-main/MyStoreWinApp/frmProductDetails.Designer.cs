﻿
namespace SalesWinApp
{
    partial class frmProductDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label2 = new System.Windows.Forms.Label();
            txtWeight = new System.Windows.Forms.TextBox();
            txtProductName = new System.Windows.Forms.TextBox();
            txtCategoryID = new System.Windows.Forms.TextBox();
            label5 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            txtUnitsInStock = new System.Windows.Forms.TextBox();
            txtUnitPrice = new System.Windows.Forms.TextBox();
            label3 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            btnCancel = new System.Windows.Forms.Button();
            btnSave = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            textProId = new System.Windows.Forms.TextBox();
            SuspendLayout();
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label2.Location = new System.Drawing.Point(64, 166);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(56, 20);
            label2.TabIndex = 38;
            label2.Text = "Weight";
            // 
            // txtWeight
            // 
            txtWeight.BackColor = System.Drawing.SystemColors.Window;
            txtWeight.Location = new System.Drawing.Point(185, 164);
            txtWeight.Multiline = true;
            txtWeight.Name = "txtWeight";
            txtWeight.Size = new System.Drawing.Size(242, 26);
            txtWeight.TabIndex = 37;
            // 
            // txtProductName
            // 
            txtProductName.BackColor = System.Drawing.SystemColors.Window;
            txtProductName.Location = new System.Drawing.Point(185, 121);
            txtProductName.Multiline = true;
            txtProductName.Name = "txtProductName";
            txtProductName.Size = new System.Drawing.Size(242, 26);
            txtProductName.TabIndex = 36;
            // 
            // txtCategoryID
            // 
            txtCategoryID.BackColor = System.Drawing.SystemColors.Window;
            txtCategoryID.Location = new System.Drawing.Point(185, 78);
            txtCategoryID.Multiline = true;
            txtCategoryID.Name = "txtCategoryID";
            txtCategoryID.Size = new System.Drawing.Size(242, 26);
            txtCategoryID.TabIndex = 35;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label5.Location = new System.Drawing.Point(64, 123);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(100, 20);
            label5.TabIndex = 34;
            label5.Text = "ProductName";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label4.Location = new System.Drawing.Point(64, 80);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(88, 20);
            label4.TabIndex = 33;
            label4.Text = "Category ID";
            // 
            // txtUnitsInStock
            // 
            txtUnitsInStock.BackColor = System.Drawing.SystemColors.Window;
            txtUnitsInStock.Location = new System.Drawing.Point(185, 250);
            txtUnitsInStock.Multiline = true;
            txtUnitsInStock.Name = "txtUnitsInStock";
            txtUnitsInStock.Size = new System.Drawing.Size(242, 26);
            txtUnitsInStock.TabIndex = 42;
            // 
            // txtUnitPrice
            // 
            txtUnitPrice.BackColor = System.Drawing.SystemColors.Window;
            txtUnitPrice.Location = new System.Drawing.Point(185, 207);
            txtUnitPrice.Multiline = true;
            txtUnitPrice.Name = "txtUnitPrice";
            txtUnitPrice.Size = new System.Drawing.Size(242, 26);
            txtUnitPrice.TabIndex = 41;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label3.Location = new System.Drawing.Point(64, 252);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(90, 20);
            label3.TabIndex = 40;
            label3.Text = "UnitsInStock";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label6.Location = new System.Drawing.Point(64, 209);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(68, 20);
            label6.TabIndex = 39;
            label6.Text = "UnitPrice";
            // 
            // btnCancel
            // 
            btnCancel.Location = new System.Drawing.Point(333, 297);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(94, 30);
            btnCancel.TabIndex = 46;
            btnCancel.Text = "Cancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // btnSave
            // 
            btnSave.Location = new System.Drawing.Point(185, 297);
            btnSave.Name = "btnSave";
            btnSave.Size = new System.Drawing.Size(94, 30);
            btnSave.TabIndex = 45;
            btnSave.Text = "Save";
            btnSave.UseVisualStyleBackColor = true;
            btnSave.Click += btnSave_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label1.Location = new System.Drawing.Point(64, 42);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(79, 20);
            label1.TabIndex = 33;
            label1.Text = "Product ID";
            // 
            // textProId
            // 
            textProId.BackColor = System.Drawing.SystemColors.Window;
            textProId.Location = new System.Drawing.Point(185, 40);
            textProId.Multiline = true;
            textProId.Name = "textProId";
            textProId.Size = new System.Drawing.Size(242, 26);
            textProId.TabIndex = 35;
            // 
            // frmProductDetails
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(538, 386);
            Controls.Add(btnCancel);
            Controls.Add(btnSave);
            Controls.Add(txtUnitsInStock);
            Controls.Add(txtUnitPrice);
            Controls.Add(label3);
            Controls.Add(label6);
            Controls.Add(label2);
            Controls.Add(txtWeight);
            Controls.Add(txtProductName);
            Controls.Add(textProId);
            Controls.Add(txtCategoryID);
            Controls.Add(label5);
            Controls.Add(label1);
            Controls.Add(label4);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "frmProductDetails";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Load += frmProductDetails_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.TextBox txtProductName;
        private System.Windows.Forms.TextBox txtCategoryID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUnitsInStock;
        private System.Windows.Forms.TextBox txtUnitPrice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textProId;
    }
}