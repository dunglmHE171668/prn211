﻿
using System.Windows.Forms;

namespace SalesWinApp
{
    partial class frmMemberDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;





        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnCancel = new Button();
            btnSave = new Button();
            cboCity = new ComboBox();
            label6 = new Label();
            label2 = new Label();
            cboCountry = new ComboBox();
            txtPassword = new TextBox();
            label1 = new Label();
            txtEmail = new TextBox();
            txtCompanyName = new TextBox();
            label5 = new Label();
            label4 = new Label();
            label3 = new Label();
            textMemId = new TextBox();
            SuspendLayout();
            // 
            // btnCancel
            // 
            btnCancel.Location = new System.Drawing.Point(335, 308);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(94, 30);
            btnCancel.TabIndex = 36;
            btnCancel.Text = "Cancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // btnSave
            // 
            btnSave.Location = new System.Drawing.Point(187, 308);
            btnSave.Name = "btnSave";
            btnSave.Size = new System.Drawing.Size(94, 30);
            btnSave.TabIndex = 35;
            btnSave.Text = "Save";
            btnSave.UseVisualStyleBackColor = true;
            btnSave.Click += btnSave_Click;
            // 
            // cboCity
            // 
            cboCity.DropDownHeight = 112;
            cboCity.FormattingEnabled = true;
            cboCity.IntegralHeight = false;
            cboCity.ItemHeight = 15;
            cboCity.Items.AddRange(new object[] { "Berlin", "Chicago", "Ha Noi", "Jakarta", "Kuala Lumpur", "Liverpool", "London", "Manchester", "Melbourne", "New York", "Seattle", "Sydney", "TPHCM", "Tokyo", "Wollongong" });
            cboCity.Location = new System.Drawing.Point(187, 257);
            cboCity.Name = "cboCity";
            cboCity.Size = new System.Drawing.Size(242, 23);
            cboCity.TabIndex = 34;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label6.Location = new System.Drawing.Point(66, 215);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(60, 20);
            label6.TabIndex = 33;
            label6.Text = "Country";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label2.Location = new System.Drawing.Point(66, 172);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(70, 20);
            label2.TabIndex = 32;
            label2.Text = "Password";
            // 
            // cboCountry
            // 
            cboCountry.DropDownHeight = 112;
            cboCountry.FormattingEnabled = true;
            cboCountry.IntegralHeight = false;
            cboCountry.ItemHeight = 15;
            cboCountry.Items.AddRange(new object[] { "Australia", "Denmark", "Germany", "India", "Indonesia", "Japan", "Malaysia", "Singapore", "Sweden", "Taiwan", "United Kingdom", "United States", "Viet Nam" });
            cboCountry.Location = new System.Drawing.Point(187, 213);
            cboCountry.Name = "cboCountry";
            cboCountry.Size = new System.Drawing.Size(242, 23);
            cboCountry.TabIndex = 31;
            // 
            // txtPassword
            // 
            txtPassword.BackColor = System.Drawing.SystemColors.Window;
            txtPassword.Location = new System.Drawing.Point(187, 170);
            txtPassword.Name = "txtPassword";
            txtPassword.Size = new System.Drawing.Size(242, 23);
            txtPassword.TabIndex = 30;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label1.Location = new System.Drawing.Point(66, 259);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(34, 20);
            label1.TabIndex = 29;
            label1.Text = "City";
            // 
            // txtEmail
            // 
            txtEmail.BackColor = System.Drawing.SystemColors.Window;
            txtEmail.Location = new System.Drawing.Point(187, 127);
            txtEmail.Name = "txtEmail";
            txtEmail.Size = new System.Drawing.Size(242, 23);
            txtEmail.TabIndex = 28;
            // 
            // txtCompanyName
            // 
            txtCompanyName.BackColor = System.Drawing.SystemColors.Window;
            txtCompanyName.Location = new System.Drawing.Point(187, 84);
            txtCompanyName.Name = "txtCompanyName";
            txtCompanyName.Size = new System.Drawing.Size(242, 23);
            txtCompanyName.TabIndex = 26;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label5.Location = new System.Drawing.Point(66, 129);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(46, 20);
            label5.TabIndex = 25;
            label5.Text = "Email";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label4.Location = new System.Drawing.Point(66, 86);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(116, 20);
            label4.TabIndex = 24;
            label4.Text = "Company Name";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label3.Location = new System.Drawing.Point(66, 49);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(24, 20);
            label3.TabIndex = 24;
            label3.Text = "ID";
            label3.Click += label3_Click;
            // 
            // textMemId
            // 
            textMemId.BackColor = System.Drawing.SystemColors.Window;
            textMemId.Location = new System.Drawing.Point(187, 47);
            textMemId.Name = "textMemId";
            textMemId.Size = new System.Drawing.Size(242, 23);
            textMemId.TabIndex = 26;
            // 
            // frmMemberDetails
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(604, 407);
            Controls.Add(btnCancel);
            Controls.Add(btnSave);
            Controls.Add(cboCity);
            Controls.Add(label6);
            Controls.Add(label2);
            Controls.Add(cboCountry);
            Controls.Add(txtPassword);
            Controls.Add(label1);
            Controls.Add(txtEmail);
            Controls.Add(textMemId);
            Controls.Add(txtCompanyName);
            Controls.Add(label5);
            Controls.Add(label3);
            Controls.Add(label4);
            FormBorderStyle = FormBorderStyle.Fixed3D;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "frmMemberDetails";
            Text = "Form1";
            Load += frmMemberDetails_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnCancel;
        private Button btnSave;
        private ComboBox cboCity;
        private Label label6;
        private Label label2;
        private ComboBox cboCountry;
        private TextBox txtPassword;
        private Label label1;
        private TextBox txtEmail;
        private TextBox txtCompanyName;
        private Label label5;
        private Label label4;
        private Label label3;
        private TextBox textMemId;
    }
}