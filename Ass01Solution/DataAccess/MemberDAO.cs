﻿using BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess
{
    public class MemberDAO
    {


        //Using Singleton Pattern
        private static MemberDAO instance = null;
        private static readonly object instanceLock = new object();
        private List<MemberObject> MemberList { get; }

        private MemberDAO()
        {
            MemberList = new List<MemberObject>()
        {
            new MemberObject
            {
                MemberID = 1,
                MemberName = "Le Dung",
                Email = "dunglmhe171668@fpt.edu.vn",
                City = "Ho Chi Minh",
                Country = "Viet Nam",
                Password = "12345678"
            },
            new MemberObject{
                MemberID=2,
                MemberName="donny le",
                Email="donny123@gmail.com",
                City="Ho Chi Minh",
                Country="United State",
                Password="123456789"
            },
            new MemberObject{
                MemberID=3,
                MemberName="Zack snyder",
                Email="Zack123@gmail.com",
                City="Ho Chi Minh",
                Country="Viet Nam",
                Password="12345678911" },
            new MemberObject{
                MemberID=5,
                MemberName="Joey vo",
                Email="Joey123@gmail.com",
                City="Ho Chi Minh",
                Country="United State",
                Password="12345678978"
            },
            new MemberObject{
                MemberID=4,
                MemberName="huy",
                Email="huy123@gmail.com",
                City="Ho Chi Minh",
                Country="United State",
                Password="12345678978"
            },


         };
        }

        public static MemberDAO Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new MemberDAO();
                    }
                    return instance;
                }
            }
        }

        public List<MemberObject> GetMemberList => MemberList;

        public MemberObject GetMemberByID(int memberID)
        {
            return MemberList.SingleOrDefault(member => member.MemberID == memberID);
        }

        public MemberObject GetMemberByName(string memberName)
        {
            return MemberList.SingleOrDefault(member => member.MemberName == memberName);
        }

        public void AddNew(MemberObject member)
        {
            if (GetMemberByID(member.MemberID) == null)
            {
                MemberList.Add(member);
            }
            else
            {
                throw new Exception("Member already exists.");
            }
        }

        public void Update(MemberObject member)
        {
            MemberObject existingMember = GetMemberByID(member.MemberID);
            if (existingMember != null)
            {
                int index = MemberList.IndexOf(existingMember);
                MemberList[index] = member;
            }
            else
            {
                throw new Exception("Member does not exist.");
            }
        }

        public void Remove(int memberID)
        {
            MemberObject member = GetMemberByID(memberID);
            if (member != null)
            {
                MemberList.Remove(member);
            }
            else
            {
                throw new Exception("Member does not exist.");
            }
        }

        public List<MemberObject> GetMemberByCityAndCountry(string city, string country)
        {
            return MemberList.FindAll(member => member.City == city && member.Country == country);
        }
    }
}

