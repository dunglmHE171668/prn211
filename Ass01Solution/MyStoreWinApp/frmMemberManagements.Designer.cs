﻿namespace MyStoreWinApp
{
    partial class frmMemberManagements
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            dgvMemberList = new DataGridView();
            btnLoad = new Button();
            btnNew = new Button();
            btnDelete = new Button();
            label1 = new Label();
            txtMemberID = new TextBox();
            txtSearch = new TextBox();
            cboSearchCountry = new ComboBox();
            cboSearchCity = new ComboBox();
            btnSearch = new Button();
            btnFind = new Button();
            button6 = new Button();
            comboBox4 = new ComboBox();
            label2 = new Label();
            txtMemberName = new TextBox();
            label3 = new Label();
            txtPassword = new TextBox();
            label4 = new Label();
            label5 = new Label();
            label6 = new Label();
            txtEmail = new TextBox();
            btnClose = new Button();
            cboCountry = new ComboBox();
            comboBox5 = new ComboBox();
            cboCity = new ComboBox();
            comboBox7 = new ComboBox();
            ((System.ComponentModel.ISupportInitialize)dgvMemberList).BeginInit();
            SuspendLayout();
            // 
            // dgvMemberList
            // 
            dgvMemberList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvMemberList.Location = new Point(12, 12);
            dgvMemberList.Name = "dgvMemberList";
            dgvMemberList.RowTemplate.Height = 25;
            dgvMemberList.Size = new Size(776, 150);
            dgvMemberList.TabIndex = 0;
            // 
            // btnLoad
            // 
            btnLoad.Location = new Point(28, 180);
            btnLoad.Name = "btnLoad";
            btnLoad.Size = new Size(75, 23);
            btnLoad.TabIndex = 1;
            btnLoad.Text = "&Load";
            btnLoad.UseVisualStyleBackColor = true;
            btnLoad.Click += btnLoad_Click;
            // 
            // btnNew
            // 
            btnNew.Location = new Point(109, 180);
            btnNew.Name = "btnNew";
            btnNew.Size = new Size(75, 23);
            btnNew.TabIndex = 1;
            btnNew.Text = "&New";
            btnNew.UseVisualStyleBackColor = true;
            btnNew.Click += btnNew_Click;
            // 
            // btnDelete
            // 
            btnDelete.Location = new Point(190, 180);
            btnDelete.Name = "btnDelete";
            btnDelete.Size = new Size(75, 23);
            btnDelete.TabIndex = 1;
            btnDelete.Text = "&Delete";
            btnDelete.UseVisualStyleBackColor = true;
            btnDelete.Click += btnDelete_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(47, 301);
            label1.Name = "label1";
            label1.Size = new Size(66, 15);
            label1.TabIndex = 2;
            label1.Text = "Member ID";
            // 
            // txtMemberID
            // 
            txtMemberID.Location = new Point(99, 298);
            txtMemberID.Name = "txtMemberID";
            txtMemberID.Size = new Size(214, 23);
            txtMemberID.TabIndex = 3;
            // 
            // txtSearch
            // 
            txtSearch.Location = new Point(109, 224);
            txtSearch.Name = "txtSearch";
            txtSearch.PlaceholderText = "ID or Name";
            txtSearch.Size = new Size(438, 23);
            txtSearch.TabIndex = 3;
            // 
            // cboSearchCountry
            // 
            cboSearchCountry.FormattingEnabled = true;
            cboSearchCountry.Items.AddRange(new object[] { "United State", "Viet Nam", "America", "United Kingdom" });
            cboSearchCountry.Location = new Point(109, 253);
            cboSearchCountry.Name = "cboSearchCountry";
            cboSearchCountry.Size = new Size(204, 23);
            cboSearchCountry.TabIndex = 4;
            cboSearchCountry.Text = "Country";
            cboSearchCountry.SelectedIndexChanged += cboSearchCountry_SelectedIndexChanged;
            // 
            // cboSearchCity
            // 
            cboSearchCity.FormattingEnabled = true;
            cboSearchCity.Items.AddRange(new object[] { "Ho Chi Minh", "Da Nang", "Ha Noi", "Phu Quoc", "Vung Tau" });
            cboSearchCity.Location = new Point(343, 253);
            cboSearchCity.Name = "cboSearchCity";
            cboSearchCity.Size = new Size(204, 23);
            cboSearchCity.TabIndex = 4;
            cboSearchCity.Text = "City";
            cboSearchCity.SelectedIndexChanged += cboSearchCity_SelectedIndexChanged;
            // 
            // btnSearch
            // 
            btnSearch.Location = new Point(608, 228);
            btnSearch.Name = "btnSearch";
            btnSearch.Size = new Size(75, 23);
            btnSearch.TabIndex = 1;
            btnSearch.Text = "&Search";
            btnSearch.UseVisualStyleBackColor = true;
            btnSearch.Click += btnSearch_Click;
            // 
            // btnFind
            // 
            btnFind.Location = new Point(609, 261);
            btnFind.Name = "btnFind";
            btnFind.Size = new Size(75, 23);
            btnFind.TabIndex = 1;
            btnFind.Text = "&Find";
            btnFind.UseVisualStyleBackColor = true;
            btnFind.Click += btnFind_Click;
            // 
            // button6
            // 
            button6.Location = new Point(1027, 309);
            button6.Name = "button6";
            button6.Size = new Size(75, 23);
            button6.TabIndex = 1;
            button6.Text = "button1";
            button6.UseVisualStyleBackColor = true;
            // 
            // comboBox4
            // 
            comboBox4.FormattingEnabled = true;
            comboBox4.Location = new Point(1180, 382);
            comboBox4.Name = "comboBox4";
            comboBox4.Size = new Size(204, 23);
            comboBox4.TabIndex = 4;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(47, 341);
            label2.Name = "label2";
            label2.Size = new Size(87, 15);
            label2.TabIndex = 2;
            label2.Text = "Member Name";
            // 
            // txtMemberName
            // 
            txtMemberName.Location = new Point(99, 338);
            txtMemberName.Name = "txtMemberName";
            txtMemberName.Size = new Size(214, 23);
            txtMemberName.TabIndex = 3;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(47, 387);
            label3.Name = "label3";
            label3.Size = new Size(57, 15);
            label3.TabIndex = 2;
            label3.Text = "Password";
            // 
            // txtPassword
            // 
            txtPassword.Location = new Point(99, 384);
            txtPassword.Name = "txtPassword";
            txtPassword.ReadOnly = true;
            txtPassword.Size = new Size(214, 23);
            txtPassword.TabIndex = 3;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(350, 301);
            label4.Name = "label4";
            label4.Size = new Size(36, 15);
            label4.TabIndex = 2;
            label4.Text = "Email";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(350, 341);
            label5.Name = "label5";
            label5.Size = new Size(50, 15);
            label5.TabIndex = 2;
            label5.Text = "Country";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(350, 387);
            label6.Name = "label6";
            label6.Size = new Size(28, 15);
            label6.TabIndex = 2;
            label6.Text = "City";
            // 
            // txtEmail
            // 
            txtEmail.Location = new Point(402, 298);
            txtEmail.Name = "txtEmail";
            txtEmail.Size = new Size(214, 23);
            txtEmail.TabIndex = 3;
            // 
            // btnClose
            // 
            btnClose.Location = new Point(643, 415);
            btnClose.Name = "btnClose";
            btnClose.Size = new Size(75, 23);
            btnClose.TabIndex = 1;
            btnClose.Text = "&Close";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // cboCountry
            // 
            cboCountry.FormattingEnabled = true;
            cboCountry.Location = new Point(405, 341);
            cboCountry.Name = "cboCountry";
            cboCountry.Size = new Size(204, 23);
            cboCountry.TabIndex = 4;
            // 
            // comboBox5
            // 
            comboBox5.FormattingEnabled = true;
            comboBox5.Location = new Point(1476, 470);
            comboBox5.Name = "comboBox5";
            comboBox5.Size = new Size(204, 23);
            comboBox5.TabIndex = 4;
            // 
            // cboCity
            // 
            cboCity.FormattingEnabled = true;
            cboCity.Location = new Point(405, 383);
            cboCity.Name = "cboCity";
            cboCity.Size = new Size(204, 23);
            cboCity.TabIndex = 4;
            // 
            // comboBox7
            // 
            comboBox7.FormattingEnabled = true;
            comboBox7.Location = new Point(1476, 512);
            comboBox7.Name = "comboBox7";
            comboBox7.Size = new Size(204, 23);
            comboBox7.TabIndex = 4;
            // 
            // frmMemberManagements
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(comboBox7);
            Controls.Add(comboBox5);
            Controls.Add(cboCity);
            Controls.Add(comboBox4);
            Controls.Add(cboCountry);
            Controls.Add(cboSearchCity);
            Controls.Add(cboSearchCountry);
            Controls.Add(txtSearch);
            Controls.Add(button6);
            Controls.Add(txtPassword);
            Controls.Add(txtMemberName);
            Controls.Add(txtEmail);
            Controls.Add(label6);
            Controls.Add(txtMemberID);
            Controls.Add(label3);
            Controls.Add(label5);
            Controls.Add(btnSearch);
            Controls.Add(label2);
            Controls.Add(label4);
            Controls.Add(btnClose);
            Controls.Add(btnFind);
            Controls.Add(label1);
            Controls.Add(btnDelete);
            Controls.Add(btnNew);
            Controls.Add(btnLoad);
            Controls.Add(dgvMemberList);
            Name = "frmMemberManagements";
            Text = "frmMemberManagements";
            Load += frmMemberManagements_Load;
            ((System.ComponentModel.ISupportInitialize)dgvMemberList).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private DataGridView dgvMemberList;
        private Button btnLoad;
        private Button btnNew;
        private Button btnDelete;
        private Label label1;
        private TextBox txtMemberID;
        private TextBox txtSearch;
        private ComboBox cboSearchCountry;
        private ComboBox cboSearchCity;
        private Button btnSearch;
        private Button btnFind;
        private Button button6;
        private ComboBox comboBox4;
        private Label label2;
        private TextBox txtMemberName;
        private Label label3;
        private TextBox txtPassword;
        private Label label4;
        private Label label5;
        private Label label6;
        private TextBox txtEmail;
        private Button btnClose;
        private ComboBox cboCountry;
        private ComboBox comboBox5;
        private ComboBox cboCity;
        private ComboBox comboBox7;
    }
}