﻿using BusinessObject;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyStoreWinApp
{
    public partial class frmMemberDetails : Form
    {
        //-----------------------------------------
        public IMemberRepository MemberRepository { get; set; }
        public bool InsertOrUpdate { get; set; }
        public MemberObject MemberInfor { get; set; }
        //----------------------------------------------
        public frmMemberDetails()
        {
            InitializeComponent();
        }

        private void frmMemberDetails_Load(object sender, EventArgs e)
        {

            cboCity.SelectedIndex = 0;
            txtMemberID.Enabled = !InsertOrUpdate;
            if (InsertOrUpdate == true)//update mode
            {
                //Show member to perform updating
                txtMemberID.Text = MemberInfor.MemberID.ToString();
                txtMemberName.Text = MemberInfor.MemberName;
                cboCity.Text = MemberInfor.City;
                txtEmail.Text = MemberInfor.Email;
                cboCountry.Text = MemberInfor.Country;
                txtPassword.Text = MemberInfor.Password;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if any required field is null or empty
                if (string.IsNullOrEmpty(txtMemberID.Text) ||
                    string.IsNullOrEmpty(txtMemberName.Text) ||
                    string.IsNullOrEmpty(cboCity.Text) ||
                    string.IsNullOrEmpty(txtEmail.Text) ||
                    string.IsNullOrEmpty(cboCountry.Text) ||
                    string.IsNullOrEmpty(txtPassword.Text))
                {
                    MessageBox.Show("Please fill in all required fields.", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return; // Do nothing if any required field is empty
                }

                // Check if the email ends with "@gmail.com"
                if (!txtEmail.Text.EndsWith("@gmail.com", StringComparison.OrdinalIgnoreCase))
                {
                    MessageBox.Show("Email must end with @gmail.com", "Invalid Email", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return; 
                }

                var member = new MemberObject
                {
                    MemberID = int.Parse(txtMemberID.Text),
                    MemberName = txtMemberName.Text,
                    City = cboCity.Text,
                    Email = txtEmail.Text,
                    Country = cboCountry.Text,
                    Password = txtPassword.Text,
                };

                if (InsertOrUpdate == false)
                {
                    // Check if the MemberID already exists
                    if (MemberIDExists(member.MemberID))
                    {
                        MessageBox.Show("Member ID already exists. Please choose a different ID.", "Duplicate ID", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    MemberRepository.InsertMember(member);
                }
                else
                {
                    MemberRepository.UpdateMember(member);
                }

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, InsertOrUpdate == false ? "somthing arent right check again" : "na ni");
            }
        }
        private bool MemberIDExists(int memberId)
        {
            // Check if the given MemberID already exists in the list
            return MemberRepository.GetMembers().Any(m => m.MemberID == memberId);
        }

        private void btnCancel_Click(object sender, EventArgs e) => Close();


    }
}
