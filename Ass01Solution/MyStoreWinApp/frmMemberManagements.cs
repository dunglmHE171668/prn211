﻿using BusinessObject;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyStoreWinApp
{
    public partial class frmMemberManagements : Form
    {
        public bool isAdmin { get; set; }
        IMemberRepository memberRepository = new MemberRepository();
        BindingSource source;

        public frmMemberManagements()
        {
            InitializeComponent();
        }

        private void frmMemberManagements_Load(object sender, EventArgs e)
        {
            SetupFormBasedOnUserRole();
        }

        private void SetupFormBasedOnUserRole()
        {
            if (isAdmin == false)
            {
                DisableFormControls();
                dgvMemberList.CellDoubleClick -= DgvMemberList_CellDoubleClick;
            }
            else
            {
                DisableFormControls();
                dgvMemberList.CellDoubleClick += DgvMemberList_CellDoubleClick;
            }
        }

        private void DisableFormControls()
        {
            btnDelete.Enabled = false;
            btnNew.Enabled = false;
            cboCity.Enabled = false;
            cboCountry.Enabled = false;
            txtEmail.Enabled = false;
            txtMemberID.Enabled = false;
            txtMemberName.Enabled = false;
            txtPassword.Enabled = false;
            btnDelete.Enabled = false;
            btnFind.Enabled = false;
            cboSearchCity.Enabled = false;
            cboSearchCountry.Enabled = false;
        }

        private void DgvMemberList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            OpenMemberDetailsFormForUpdate();
        }

        private void OpenMemberDetailsFormForUpdate()
        {
            frmMemberDetails frmMemberDetails = new frmMemberDetails
            {
                Text = "Update member",
                InsertOrUpdate = true,
                MemberInfor = GetMemberObject(),
                MemberRepository = memberRepository
            };

            if (frmMemberDetails.ShowDialog() == DialogResult.OK)
            {
                LoadMemberList();
                source.Position = source.Count - 1;
            }
        }

        private MemberObject GetMemberObject()
        {
            MemberObject member = null;
            try
            {
                member = new MemberObject
                {
                    MemberID = int.Parse(txtMemberID.Text),
                    MemberName = txtMemberName.Text,
                    Password = txtPassword.Text,
                    Email = txtEmail.Text,
                    Country = cboCountry.Text,
                    City = cboCity.Text,
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Get member");
            }
            return member;
        }

        public void LoadMemberList()
        {
            var members = memberRepository.GetMembers();

            try
            {
                source = new BindingSource();
                source.DataSource = members.OrderByDescending(member => member.MemberName);

                SetupDataBindings();

                dgvMemberList.DataSource = null;
                dgvMemberList.DataSource = source;

                EnableOrDisableDeleteButton();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load member list");
            }
        }

        private void SetupDataBindings()
        {
            txtMemberID.DataBindings.Clear();
            txtMemberName.DataBindings.Clear();
            txtPassword.DataBindings.Clear();
            txtEmail.DataBindings.Clear();
            cboCountry.DataBindings.Clear();
            cboCity.DataBindings.Clear();

            txtMemberID.DataBindings.Add("Text", source, "MemberID");
            txtMemberName.DataBindings.Add("Text", source, "MemberName");
            txtPassword.DataBindings.Add("Text", source, "Password");
            txtEmail.DataBindings.Add("Text", source, "Email");
            cboCountry.DataBindings.Add("Text", source, "Country");
            cboCity.DataBindings.Add("Text", source, "City");
        }

        private void EnableOrDisableDeleteButton()
        {
            if (isAdmin == false)
            {
                if (source.Count == 0)
                {
                    ClearText();
                    btnDelete.Enabled = false;
                }
                else
                {
                    btnDelete.Enabled = false;
                }
            }
            else
            {
                btnNew.Enabled = true;
                btnFind.Enabled = true;
                cboSearchCity.Enabled = true;
                cboSearchCountry.Enabled = true;
                if (source.Count == 0)
                {
                    ClearText();
                    btnDelete.Enabled = false;
                }
                else
                {
                    btnDelete.Enabled = true;
                    
                }
            }
        }

        private void ClearText()
        {
            txtMemberID.Text = string.Empty;
            txtMemberName.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtPassword.Text = string.Empty;
            cboCountry.Text = string.Empty;
            cboCity.Text = string.Empty;
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            LoadMemberList();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            OpenMemberDetailsFormToAdd();
        }

        private void OpenMemberDetailsFormToAdd()
        {
            frmMemberDetails frmMemberDetails = new frmMemberDetails
            {
                Text = "Add member",
                InsertOrUpdate = false,
                MemberRepository = memberRepository
            };

            if (frmMemberDetails.ShowDialog() == DialogResult.OK)
            {
                LoadMemberList();
                source.Position = source.Count - 1;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteMember();
        }

        private void DeleteMember()
        {
            try
            {
                var member = GetMemberObject();
                memberRepository.DeleteMember(member.MemberID);
                LoadMemberList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Delete a member");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadOneMember()
        {
            string searchText = txtSearch.Text.ToLower(); // Convert input to lowercase for case-insensitive comparison

            if (!string.IsNullOrEmpty(searchText))
            {
                var members = memberRepository.GetMembers();

                var searchResults = members
                    .Where(m => m.MemberName.ToLower().Contains(searchText) || m.MemberID.ToString().Contains(searchText))
                    .ToList();

                if (searchResults.Any())
                {
                    dgvMemberList.DataSource = searchResults;
                }
                else
                {
                    MessageBox.Show("No matching members found.", "Search Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                // If the search text is empty, show all members
                dgvMemberList.DataSource = memberRepository.GetMembers();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadOneMember();
        }

        private void FilterMember()
        {
            List<MemberObject> filterList = memberRepository.GetMemberByCityAndCountry(cboSearchCity.Text, cboSearchCountry.Text);

            try
            {
                if (filterList.Count == 0)
                {
                    MessageBox.Show("No member matched", "No result");
                }
                else
                {
                    source = new BindingSource();
                    source.DataSource = filterList.OrderByDescending(member => member.MemberName);

                    SetupDataBindings();

                    dgvMemberList.DataSource = null;
                    dgvMemberList.DataSource = source;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load member list");
            }
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            FilterMember();
        }

        private void cboSearchCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Handle the selected index changed event of the search city combo box

            string selectedCity = cboSearchCity.SelectedItem?.ToString();
            string selectedCountry = cboSearchCountry.SelectedItem?.ToString();

            // Check if both city and country are selected
            if (!string.IsNullOrEmpty(selectedCity) && !string.IsNullOrEmpty(selectedCountry))
            {
                FilterMembersByCityAndCountry(selectedCity, selectedCountry);
            }
        }

        private void cboSearchCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Handle the selected index changed event of the search country combo box

            string selectedCity = cboSearchCity.SelectedItem?.ToString();
            string selectedCountry = cboSearchCountry.SelectedItem?.ToString();

            // Check if both city and country are selected
            if (!string.IsNullOrEmpty(selectedCity) && !string.IsNullOrEmpty(selectedCountry))
            {
                FilterMembersByCityAndCountry(selectedCity, selectedCountry);
            }
        }

        private void FilterMembersByCityAndCountry(string city, string country)
        {
            // Filter members based on the selected city and country
            var filteredMembers = memberRepository.GetMembers()
                .Where(m => m.City.Equals(city, StringComparison.OrdinalIgnoreCase) && m.Country.Equals(country, StringComparison.OrdinalIgnoreCase))
                .ToList();

            // Update the DataGridView with the filtered members
            dgvMemberList.DataSource = filteredMembers;
        }



    }

}
