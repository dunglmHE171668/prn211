using BusinessObject;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json; // Use System.Text.Json instead of System.Web.Script.Serialization
using System.Windows.Forms;

namespace MyStoreWinApp
{
    public partial class frmLogin : Form
    {
        private MemberRepository memberRepository = new MemberRepository();

        public frmLogin()
        {
            InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            string json = string.Empty;

            // Use System.Text.Json.JsonSerializer instead of JavaScriptSerializer
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };

            // Use absolute path to avoid issues with file location
            string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "appsettings.json");

            try
            {
                json = File.ReadAllText(filePath);

                var obj = JsonSerializer.Deserialize<JsonElement>(json, options);

                // Check if keys exist in the dictionary before accessing them
                if (obj.TryGetProperty("DefaultAccount", out var defaultAccount))
                {
                    var admin = new MemberObject
                    {
                        // Access elements using GetProperty method
                        Email = defaultAccount.GetProperty("Email").GetString(),
                        Password = defaultAccount.GetProperty("password").GetString()
                    };

                    var members = memberRepository.GetMembers();
                    bool canLog = false;

                    foreach (var i in members)
                    {
                        if (i.MemberName.Equals(txtUserName.Text) && i.Password.Equals(txtPassword.Text))
                        {
                            OpenMemberManagementForm(isAdmin: false);
                            canLog = true;
                            this.Close();
                        }
                        else if (admin.Email.Equals(txtUserName.Text) && admin.Password.Equals(txtPassword.Text))
                        {
                            OpenMemberManagementForm(isAdmin: true);
                            this.Close();
                        }
                    }

                    if (!canLog)
                    {
                        MessageBox.Show("Can not find member", "Error");
                    }
                }
                else
                {
                    MessageBox.Show("DefaultAccount key not present in appsettings.json", "Error");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error reading appsettings.json: {ex.Message}", "Error");
            }
        }


        private void btnCancel_Click(object sender, EventArgs e) => this.Close();

        private void OpenMemberManagementForm(bool isAdmin)
        {
            frmMemberManagements frm = new frmMemberManagements()
            {
                isAdmin = isAdmin
            };
            frm.ShowDialog();
        }

    }
}
