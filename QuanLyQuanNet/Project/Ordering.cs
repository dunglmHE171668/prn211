﻿using Microsoft.VisualBasic.Devices;
using Project.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project
{
    public partial class Ordering : Form
    {
        public Ordering()
        {
            InitializeComponent();
        }
        private QuanLyQuanNetContext context = new QuanLyQuanNetContext();
        private void Order_Load(object sender, EventArgs e)
        {
            Load_Data();
        }

        public void Load_Data()
        {
            var result = (
                  from o in context.Orders
                  join c in context.Computers
                  on o.ComputerId equals c.Id
                  join f in context.FoodServices
                  on o.ServiceId equals f.Id
                  select new
                  {
                      o.Id,
                      ComputerName = c.ComputerName,
                      Service = f.ServiceName,
                      o.OrderDate,
                      o.Quantity

                  }).ToList();
            dataGridView1.DataSource = result;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            int orderId = Convert.ToInt32(txbOrderId.Text);

            if (txbOrderId.Text != null)
            {
                // Retrieve the selected order from the database
                var orderToUpdate = context.Orders.FirstOrDefault(p => p.Id == orderId);

                if (orderToUpdate != null)
                {
                    // Make changes to the order based on your requirements
                    orderToUpdate.Quantity = Convert.ToInt32(txbQuantity.Text);

                    
                    // Save the changes to the database
                    context.SaveChanges();

                    DialogResult res;
                    res = MessageBox.Show("Update Successful", "Update", MessageBoxButtons.OK);

                    if (res == DialogResult.OK)
                    {
                        Load_Data(); // Reload the data to refresh the DataGridView
                    }
                }
                else
                {
                    MessageBox.Show("Order not found!");
                }
            }
            else
            {
                MessageBox.Show("Please choose 1 order!");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string orderIdText = txbOrderId.Text;
            if (int.TryParse(orderIdText, out int orderId))
            {
                string computerName = txbComputer.Text;
                if (orderIdText != null)
                {
                    if (cbxService.Text == "open computer")
                    {
                        var computerToUpdate = context.Computers.FirstOrDefault(p => p.ComputerName == computerName);
                        if (computerToUpdate != null)
                        {
                            computerToUpdate.Status = "inactive";
                            context.SaveChanges();
                        }
                    }

                    var orderToDelete = context.Orders.FirstOrDefault(p => p.Id == orderId);
                    if (orderToDelete != null)
                    {
                        context.Orders.Remove(orderToDelete);
                        context.SaveChanges();

                        txbOrderId.Text = null;
                        txbComputer.Text = null;
                        txbQuantity.Text = null;
                        cbxService.Text = null;

                        DialogResult res;
                        res = MessageBox.Show("Delete Successful", "Exit", MessageBoxButtons.OK);
                        if (res == DialogResult.OK)
                        {
                            Load_Data();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Order not found!");
                    }
                }
                else
                {
                    MessageBox.Show("Please choose 1 order!");
                }
            }
            else
            {
                MessageBox.Show("Invalid order ID. Please enter a valid number.");
            }
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult res;
            res = MessageBox.Show("Do you want to exit", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.Yes)
            {
                this.Close();
            }
            else
            {
                this.Show();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = dataGridView1.CurrentRow.Index;
            txbOrderId.Text = dataGridView1.Rows[i].Cells[0].Value.ToString();
            txbComputer.Text = dataGridView1.Rows[i].Cells[1].Value.ToString();
            cbxService.Text = dataGridView1.Rows[i].Cells[2].Value.ToString();
            txbQuantity.Text = dataGridView1.Rows[i].Cells[4].Value.ToString();
        }
    }
}
